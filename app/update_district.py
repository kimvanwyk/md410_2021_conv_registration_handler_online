""" Set the district for a supplied MDC2021 registration number

"""
from md410_2021_conv_common_online import db


def update_district(reg_num, district):
    dbh = db.DB()
    registree = dbh.get_registree(reg_num)
    registree.district = district
    dbh.save_registree(registree)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "reg_num",
        type=int,
        help="The registration number to download data from or reprocess",
    )
    parser.add_argument(
        "district",
        help="District value to use",
    )
    args = parser.parse_args()
    update_district(args.reg_num, args.district)
