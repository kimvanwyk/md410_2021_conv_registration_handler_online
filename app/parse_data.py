""" Parse reg form from the MD410 2021 convention website and upload to the central database
"""

import attr
from dateutil.parser import parse

from md410_2021_conv_common_online import db

import json
import os.path

BOOLS = (
    "first_mdc",
    "voter",
    "attending_district_convention",
    "attending_md_convention",
)


def parse_reg_form_fields(form_data, out_dir=None):
    data = {}
    for (k, v) in form_data.items():
        v = v.strip() if hasattr(v, "strip") else v
        if k in ("timestamp",):
            data["timestamp"] = parse(v, yearfirst=True)
        if k in ("registration_number",):
            data["reg_num"] = int(v)
        if "main_" in k:
            name = k[5:]
            if name in BOOLS:
                v = bool(v)
            data[name] = v
        if 'voter' not in data:
            data['voter'] = False
    return db.Registree(**data)


def parse_data_file(reg_form_file):
    with open(reg_form_file, "r") as fh:
        d = json.load(fh)
    registree = parse_reg_form_fields(d)
    return registree


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument("data_file", help="JSON data file to parse.")
    args = parser.parse_args()
    registree = parse_data_file(args.data_file)
    print(registree)
