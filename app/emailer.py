import os

import attr
from dotenv import load_dotenv
from mailtools import SMTPMailer

load_dotenv()


@attr.s
class Email:
    """Email messaging functions"""

    sent_from = attr.ib(default=os.getenv("EMAIL_FROM"))
    smtp_server = attr.ib(default=os.getenv("EMAIL_HOST"))
    smtp_port = attr.ib(default=os.getenv("EMAIL_PORT"))
    smtp_user = attr.ib(default=os.getenv("EMAIL_HOST_USER"))
    smtp_password = attr.ib(default=os.getenv("EMAIL_HOST_PASSWORD"))

    def __attrs_post_init__(self):
        self.mailer = SMTPMailer(
            self.smtp_server,
            self.smtp_port,
            username=self.smtp_user,
            password=self.smtp_password,
            transport_args={"security": "STARTTLS"},
        )

    def send(self, tos, subject, body, attachments=None):
        if attachments is None:
            attachments = []
        self.mailer.send_plain(
            self.sent_from,
            tos,
            subject,
            body,
            attachments=attachments,
        )


if __name__ == "__main__":
    email = Email()
    email.send(
        ["vanwyk.k@gmail.com"],
        "Imagine a great subject here",
        "This is a test email\n\nwith\n\nseveral lines.",
        ["data.json"],
    )
