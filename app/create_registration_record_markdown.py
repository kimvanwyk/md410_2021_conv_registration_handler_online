import attr
import json
import os.path

from md410_2021_conv_common_online import db


@attr.s
class RegistreeRenderer(object):
    registree = attr.ib()
    out_dir = attr.ib(default=None)

    def __make_names(self, attr):
        self.names.append()

    def __attrs_post_init__(self):
        self.name = f"{self.registree.first_names.lower().replace(' ', '_')}_{self.registree.last_name.replace(' ','_').lower()}"
        self.out = [
            f"# Registration Number: {self.registree.reg_num} {{-}}",
        ]

        self.out.append(
            f"# Attendee Details - Registered on {self.registree.timestamp:%d/%m/%y at %H:%M} {{-}}"
        )
        self.render_registree()
        self.out.append("")
        self.out.append("")
        name = self.name.replace("'", "").replace(".", "")
        self.fn = f"mdc2021_registration_{self.registree.reg_num:003}_{name}.txt"
        if self.out_dir:
            self.fn = os.path.join(self.out_dir, self.fn)

    def render_registree(self):
        self.out.append(f"* **First Name(s):** {self.registree.first_names}")
        self.out.append(f"* **Last Name:** {self.registree.last_name}")
        if self.registree.cell:
            self.out.append(f"* **Cell Phone:** {self.registree.cell}")
        if self.registree.email:
            self.out.append(f"* **Email Address:** {self.registree.email}")
        self.out.append(f"* **Club:** {self.registree.club}")
        self.out.append(f"* **District:** {self.registree.district}")
        self.out.append(
            f"* **This will be the attendee's first MD Convention:** {'Yes' if self.registree.first_mdc else 'No'}"
        )
        self.out.append(
            f"* **Attendee will exercise one of their club's votes:** {'Yes' if self.registree.voter else 'No'}"
        )
        self.out.append(
            f"* **Attendee will attend the online District {self.registree.district} convention:** {'Yes' if self.registree.attending_district_convention else 'No'}"
        )
        self.out.append(
            f"* **Attendee will attend the online Multiple District convention:** {'Yes' if self.registree.attending_md_convention else 'No'}"
        )

    def save(self):
        with open(self.fn, "w") as fh:
            print("\n".join(self.out))
            fh.write("\n".join(self.out))


def main(reg_num=None, registree=None, out_dir="."):
    if not registree:
        if reg_num is not None:
            dbh = db.DB()
            registree = dbh.get_registree(reg_num)
        else:
            raise ValueError(
                "Either a reg num to look up or a Registree should be provided"
            )
    renderer = RegistreeRenderer(registree, out_dir)

    renderer.save()
    return renderer.fn


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Build MD410 2021 Convention registration record"
    )
    parser.add_argument("reg_num", type=int, help="The registration number to use.")
    parser.add_argument(
        "--out_dir", default=".", help="The directory to write output to."
    )
    parser.add_argument("--fn", action="store_true", help="Output resulting filename")
    args = parser.parse_args()

    main(reg_num=args.reg_num, out_dir=args.out_dir)
