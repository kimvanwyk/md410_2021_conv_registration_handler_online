Good day {registree.first_names},

Thank you for registering for the 2021 Multiple District 410 Online Convention on 1 May 2021. A record of your registration details is attached. Please contact Kim van Wyk on vanwykk@gmail.com if any of these details are incorrect.

The Convention will be presented virtually, using Zoom. You can use this link to access the Convention:

https://zoom.us/j/96002258314?pwd=bVhYcG9NSDQ3bUxnbm8ycUc1cXhXZz09

If you require a passcode to access the Zoom meeting, it is 974353.

Please note that the attendance register for the minutes will be based on the list of registered delegates.  It is acceptable to attend the MD Convention as a group and share a connection rather than every attendee needing to connect with their own device. 

We would like to remind you to follow etiquette and use the mute facility at all times so as not to interfere with the recording.  Should you have questions or comments under any of the agenda points, please use the chat facility as well as the “raised hand” option at the appropriate time so that you can be acknowledged.  Please remember to state your name and club name in order for your question or comment to be accurately recorded.

Reports for the Multiple District online convention can be downloaded from https://www.lionsconvention2021.co.za/reports.

Thank you again for registering for the 2021 MD410 Convention.

The 2021 MD410 Convention Organising Committee
