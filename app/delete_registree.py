""" Delete a supplied MDC2021 registration number

"""
from md410_2021_conv_common_online import db


def delete_reg_num(reg_num):
    dbh = db.DB()
    dbh._DB__delete_registrees([reg_num])


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "reg_num",
        type=int,
        help="The registration number to delete",
    )
    args = parser.parse_args()
    delete_reg_num(args.reg_num)
