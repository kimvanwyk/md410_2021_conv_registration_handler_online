""" Obtain a reg form record for a given reg number and provide email steps
"""

import os.path

from emailer import Email
from md410_2021_conv_common_online import db
import s3

import attr
import pyperclip


BCC = ""
with open("reg_form_msg.txt", "r") as fh:
    BODY = fh.read()


def send_email(reg_num=None, registree=None, fn=None):
    if fn is None:
        s = s3.S3(reg_num)
        fn = s.download_pdf_reg_file(reg_num)

    if reg_num is not None:
        dbh = db.DB()
        registree = dbh.get_registree(reg_num)
    elif registree is None:
        raise ValueError(
            "Either a reg num to look up or a Registree should be provided"
        )

    if registree.email:
        email = Email()
        subject = f"Registration for the 2021 MD410 Online Convention for {registree.name}. Registration number: MDC{registree.reg_num:003}"
        kwds = locals()
        kwds.update(globals())
        body = BODY.format(**kwds)
        email.send([registree.email], subject, body, [os.path.abspath(fn)])
        print(f"Registration email sent to {registree.name} at {registree.email}")
    else:
        print("No email addresses supplied")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument("reg_num", type=int, help="Registration number")
    args = parser.parse_args()
    send_email(reg_num=args.reg_num)
